/**
 * Created by lijizhuang on 2017/7/28.
 */
import React, {Component} from "react";
import {Alert, StyleSheet, Dimensions, View} from "react-native";
import {Button, Header, Icon, Input, Item, Left, Right, Text} from "native-base";

import address from '../../channel/address';
import producer from '../../channel/producer';

import LoadingSpinner from "../public/loadingSpinner";
import SearchItem from "./SearchItem";
import UltimateListView from "../../react-native-ultimate-listview/ultimateListView";

const {width, height} = Dimensions.get('window');
var lastKeyword = '';
export default class SearchList extends Component {
    constructor(props) {
        super(props);
        const {type, category} = props;
        this.state = {
            layout: 'list',
            text: '',
            type: type,
            keyword: category.keyword
        }
    }

    componentWillReceiveProps(props) {
        const {category} = props;
        this.setState({
            keyword: category.keyword
        });
    }

    //sleep = (time) => new Promise(resolve => setTimeout(() => resolve(), time));

    onFetch = async (page = 1, startFetch, abortFetch) => {
        try {
            let pageLimit = 20;
            const url = address.searchKeywords(this.state.keyword, page, pageLimit, this.state.type);

            //let response = await fetch('http://suggestion.baidu.com/su?wd=12306&action=opensearch&ie=utf-8');
            //let responseJson = await response.json();
            var items = await window.fetch(url)
                .then(res => res.json())
                .then(data => {
                    if (data.msg === '页码超出查询范围' && data.code === 401)
                        return [];
                    else
                        return producer.searchGeneral(data);
                });

            startFetch(items, pageLimit);
        } catch (err) {
            abortFetch(); //manually stop the refresh or pagination if it encounters network error
        }
    };

    //onChangeLayout = (event) => {
    //    this.setState({text: ''});
    //    switch (event.nativeEvent.selectedSegmentIndex) {
    //        case 0:
    //            this.setState({layout: 'list'});
    //            break;
    //        case 1:
    //            this.setState({layout: 'grid'});
    //            break;
    //        default:
    //            break;
    //    }
    //};

    onChangeScrollToIndex = (num) => {
        this.setState({text: num});
        let index = num;
        //if (this.state.layout === 'grid') {
        //    index = num / 3;
        //}
        try {
            this.listView.scrollToIndex({viewPosition: 0, index: Math.floor(index)});
        } catch (err) {
            console.warn(err);
        }
    };

    renderItem = (item, index, separator) => {
        //if (this.state.layout === 'list') {
        return (
            <SearchItem item={item} index={index} onPress={this.onPressItem} {...this.props}/>
        );
        //} else if (this.state.layout === 'grid') {
        //    return (
        //        <FlatListGrid item={item} index={index} onPress={this.onPressItem}/>
        //    );
        //}
    };

    onPressItem = (type, index, item) => {
        Alert.alert(type, `You're pressing on ${item}`);
    };

    //renderControlTab = () => {
    //    return (
    //        <ControlTab layout={this.state.layout}
    //                    onChangeLayout={this.onChangeLayout}
    //        />
    //    );
    //};

    renderHeader = () => {
        return (
            <View>
                <View style={styles.header}>
                    <Text style={{textAlign: 'center'}}>I'm the Header View, you can put some Instructions or Ads Banner
                        here!</Text>
                </View>
                <View style={styles.headerSegment}>
                    <Left style={{flex: 0.15}}/>
                    {this.renderControlTab()}
                    <Right style={{flex: 0.15}}/>
                </View>
            </View>
        );
    };

    renderPaginationFetchingView = () => {
        return (
            <LoadingSpinner height={height * 0.2} text="努力加载中..."/>
        );
    };

    forceRefresh(needFetch){
        //强制刷新
        this.listView.forceRefresh(needFetch);
    }

    resetFirstLoader(){
        this.listView.resetFirstLoader();
    }

    render() {
        const {category, actions} = this.props;
        return (
            <View style={styles.container}>
                <UltimateListView
                    ref={(ref) => this.listView = ref}
                    key={this.state.layout} //this is important to distinguish different FlatList, default is numColumns
                    onFetch={this.onFetch}
                    keyExtractor={(item, index) => `${index} - ${item.name}`}  //this is required when you are using FlatList
                    refreshableMode="advanced" //basic or advanced
                    item={this.renderItem}  //this takes three params (item, index, separator)
                    numColumns={this.state.layout === 'list' ? 1 : 3} //to use grid layout, simply set gridColumn > 1
                    //----Extra Config----
                    //header={this.renderHeader}
                    paginationFetchingView={this.renderPaginationFetchingView}
                    //sectionHeaderView={this.renderSectionHeaderView}   //not supported on FlatList
                    //paginationFetchingView={this.renderPaginationFetchingView}
                    //paginationAllLoadedView={this.renderPaginationAllLoadedView}
                    //paginationWaitingView={this.renderPaginationWaitingView}
                    //emptyView={this.renderEmptyView}
                    //separator={this.renderSeparatorView}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});
