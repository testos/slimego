/**
 * Created by lijizhuang on 2017/7/6.
 */
'use strict';

import Channel from '../channel';
import { APP } from '../constants';

/**
 * Switch tab
 */
//const changeTab = (tab) => {
//  return (dispatch) => {
//    return Promise.resolve(dispatch({
//      type: APP.TAB,
//      data: tab
//    }))
//  }
//};

const  changeTab = function (tab) {
    return function(dispatch){
        return Promise.resolve(dispatch({
            type: APP.TAB,
            data: tab
        }));
    };
};

/**
 * Navigation, now only used for gamelist to gamedetail
 * when enter game detail, game list should stop requesting data continuously
 */
const toNavigation = targetComponent => {
    return dispatch => {
        return Promise.resolve(dispatch({
            type: APP.NAVIGATION,
            data: targetComponent
        }))
    }
};

export default {
    changeTab,
    toNavigation
}
