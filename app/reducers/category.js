/**
 * Created by lijizhuang on 2017/8/1.
 */
import createReducer from '../utils/create-reducer'

import {CATEGORY} from '../constants'

/* search result */
const initialState = {
    keyword: '',
    cateTabIndex: -1
};

const actionHandler = {
    [CATEGORY.SYNCKEYWORD]: (state, action) =>{
        return Object.assign({}, state, {
            actionType:action.type,
            keyword: action.data
        })
    },

    [CATEGORY.SWITCHTOPTAB]: (state, action) => {
        return Object.assign({}, state, {
            actionType:action.type,
            cateTabIndex: action.data
        })
    },
};

export default createReducer(initialState, actionHandler);