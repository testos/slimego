'use strict';

// React
import React from 'react'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import {bindActionCreators} from 'redux'

import { TabBar } from '../navigationConfiguration'
import homeActions from '../../actions/home'
import cateActions from '../../actions/category'
import hotsActions from '../../actions/hots'

//Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        navigationState: state.tabBar,
        home: state.home,
        category: state.category,
        hots: state.hots
    }
};

const mapDispatchToProps = (dispatch) => {
    var actions =
    {
        homeActions: bindActionCreators(Object.assign({}, homeActions), dispatch),
        cateActions: bindActionCreators(Object.assign({}, cateActions), dispatch),
        hotsActions: bindActionCreators(Object.assign({}, hotsActions), dispatch),
    };
    return {
        ...actions,
        dispatch
    }
};

export class TabBarNavigation extends React.Component {

    render() {
        const newProps = {...this.props};
        delete newProps.dispatch;
        delete newProps.navigationState;

        return (
            <TabBar
                navigation={
                  addNavigationHelpers({
                     dispatch: this.props.dispatch,
                     state: this.props.navigationState
                  })
                }

                screenProps={{...newProps}}
            />
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(TabBarNavigation);