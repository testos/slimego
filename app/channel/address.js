/**
 * Created by lijizhuang on 2017/7/7.
 */
'use strict';

const address = {
    searchKeywords: (keyword, page, rows, type) => {
        return `http://www.slimego.com/api/search?q=${keyword}&page=${page}&rows=${rows}&type=${type}`
    },

    autocomplete: (keyword)=> {
        return `http://suggestion.baidu.com/su?wd=${keyword}&action=opensearch&ie=utf-8`;
    },

    jumpResource: (uk, shareId) => {
        return `https://pan.baidu.com/share/link?uk=${uk}&third=0&shareid=${shareId}`;
    },

    hotKeywords:()=>{
        return `http://slimego.cn/api/hot`
    },

    articleList:()=>{
        return `https://mp.weixin.qq.com/s/FNATQycW0w3ii4WKg_ourA`
    }
};

export default address;