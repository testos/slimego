/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

import {AppRegistry} from 'react-native'
import Root from './app/root'

AppRegistry.registerComponent('slimego', () => Root);
